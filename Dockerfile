FROM mkroli/servicemix


ENV SERVICEMIX_VERSION_MAJOR=7
ENV SERVICEMIX_VERSION_MINOR=0
ENV SERVICEMIX_VERSION_PATCH=0.M3

# this file has version specific properties,
# check if the parent image has the same version to avoid config errors
ADD ["org.apache.karaf.features.cfg", "/opt/servicemix/etc/org.apache.karaf.features.cfg"]

# add activemq stomp transport connector
ADD ["activemq.xml", "/opt/servicemix/etc/activemq.xml"]

# Add ftp camel route
#ADD ["ftp-blueprint.xml", "/deploy/ftp-blueprint.xml"]

VOLUME ["/root/.m2/repository"]

# expose activemq stomp port
EXPOSE 61613