Apache ServiceMix :: Camel :: CXF

ServiceMix bundle that provide a CamelContext and rest interface for dynamically managing routes
==================

Prerequisites for Running the bundle
-------------------------------------
1. You must have the following installed on your machine:

   - JDK 1.6 or higher

   - Maven 2.2.1 or higher (for building)

   - Camel-Quartz bundle (for quartz source routes)

2. Start ServiceMix by running the following command:

    <servicemix_home>/bin/servicemix          (on UNIX)
    <servicemix_home>\bin\servicemix          (on Windows)


Running the bundle
-------------------

Building
---------------------------------------
To install and run the bundle, complete the following steps:

1. Build the bundle by opening a command prompt, changing directory where is pom file
   and entering the following Maven
   command:

     mvn install

   The mvn install command builds the example deployment bundle and
   copies it to your local Maven repository and to the target directory
   of this bundle.
     
2. Install the bundle by entering the following command in
   the ServiceMix console:

     bundle:install mvn:org.apache.camel/camel-cxf-routes/4.0.8

3. Run the bundle by entering the following command

    bundle:start dynamic-camel-cxf-routes
   
Once the bundle is running,you can send rest requests to access camel context


Stopping and Uninstalling the Example
-------------------------------------
To stop the bundle, enter the following command in the ServiceMix
console:

  bundle:stop start dynamic-camel-cxf-routes

To stop the bundle, enter the following command in the ServiceMix
console:

  bundle:uninstall  dynamic-camel-cxf-routes

Viewing the Log Entries
-----------------------
You can view the entries in the log file in the data/log
directory of your ServiceMix installation, or by typing
the following command in the ServiceMix console:

  log:display
