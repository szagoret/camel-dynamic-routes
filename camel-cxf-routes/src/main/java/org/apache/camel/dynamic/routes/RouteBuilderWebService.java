/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.camel.dynamic.routes;

import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.dynamic.routes.service.RouteBuilderService;
import org.apache.camel.model.RouteDefinition;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author szagoret
 */

@Path("/routeBuilderService/")
@Produces("application/json")
@Consumes("application/json")
public class RouteBuilderWebService {

    private RouteBuilderService routeBuilderService;

    @GET
    @Path("/routes/{id}/")
    public Response getRoute(@PathParam("id") String id) {
//        ChoiceCamelRoute camelRoute = new ChoiceCamelRoute();
        return Response.ok().entity("").build();
    }

    @GET
    @Path("/routes/show/")
    @Produces("text/xml")
    public List<RouteDefinition> showRoutes() throws Exception {
        return routeBuilderService.showRoutes();
    }

    @POST
    @Path("/routes/")
    public Response addRoute(CamelRoute camelRoute) {
        try {
            String response = routeBuilderService.addRoute(camelRoute);
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/routes/{id}/")
    public Response deleteRoute(@PathParam("id") String id) throws Exception {
        routeBuilderService.deleteRoute(id);
        return Response.ok().type("application/json").build();
    }

    public void setRouteBuilderService(RouteBuilderService routeBuilderService) {
        this.routeBuilderService = routeBuilderService;
    }
}
