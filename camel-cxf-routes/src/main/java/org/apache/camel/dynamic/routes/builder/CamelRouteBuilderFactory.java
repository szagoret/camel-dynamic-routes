package org.apache.camel.dynamic.routes.builder;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.dynamic.routes.model.OutputChoice;

import java.util.List;

/**
 * Created by szagoret on 27.02.2017.
 */
public class CamelRouteBuilderFactory {
    public static RouteBuilder getRouteBuilder(CamelRoute camelRoute) {
        List<OutputChoice> outputChoices = camelRoute.getOutputChoices();
        if (outputChoices != null) {
            if (outputChoices.size() > 1) {
                return new ChoiceCamelRouteBuilder(camelRoute);
            } else {
                return new SimpleCamelRouteBuilder(camelRoute);
            }
        } else {
            throw new IllegalArgumentException("CamelRoute outputs is null:" + camelRoute);
        }
    }
}
