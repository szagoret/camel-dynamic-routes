package org.apache.camel.dynamic.routes.builder;

import org.apache.camel.builder.PredicateBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.dynamic.routes.model.OutputChoice;
import org.apache.camel.model.ChoiceDefinition;
import org.apache.camel.model.RouteDefinition;

/**
 * Created by szagoret on 27.02.2017.
 */
public class ChoiceCamelRouteBuilder extends RouteBuilder {

    private final CamelRoute camelRoute;

    public ChoiceCamelRouteBuilder(CamelRoute camelRoute) {
        // check route integrity
        ValidationUtils.validateChoiceCamelRoute(camelRoute);
        this.camelRoute = camelRoute;
    }

    @Override
    public void configure() throws Exception {
        RouteDefinition routeDefinition = from(camelRoute.getFrom()).routeId(camelRoute.getRouteId());
        ChoiceDefinition choiceDefinition = routeDefinition.choice();
        for (OutputChoice outputChoice : camelRoute.getOutputChoices()) {
            ChoiceDefinition choiceWhen = choiceDefinition.when(PredicateBuilder.and(simple(outputChoice.getExpression())));
            choiceWhen.setBody(simple(outputChoice.getSimpleBody()));
            for (String output : outputChoice.getOutputs()) {
                choiceWhen.to(output);
            }
        }
    }
}
