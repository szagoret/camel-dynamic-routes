package org.apache.camel.dynamic.routes.builder;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.dynamic.routes.model.OutputChoice;
import org.apache.camel.model.RouteDefinition;

import java.util.List;

/**
 * Created by szagoret on 27.02.2017.
 */
public class SimpleCamelRouteBuilder extends RouteBuilder {

    private final CamelRoute camelRoute;

    public SimpleCamelRouteBuilder(CamelRoute camelRoute) {
        // check route integrity
        ValidationUtils.validateSimpleCamelRoute(camelRoute);
        this.camelRoute = camelRoute;
    }

    @Override
    public void configure() throws Exception {
        OutputChoice outputChoice = camelRoute.getOutputChoices().get(0);
        String routeBodyMsg = outputChoice.getSimpleBody();
        List<String> routeOutputs = outputChoice.getOutputs();
        RouteDefinition routeDefinition = from(camelRoute.getFrom()).routeId(camelRoute.getRouteId()).setBody(simple(routeBodyMsg));
        for (String output : routeOutputs) {
            routeDefinition.to(output);
        }
    }
}
