package org.apache.camel.dynamic.routes.builder;

import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.dynamic.routes.model.OutputChoice;
import org.springframework.util.Assert;

/**
 * Created by szagoret on 03.02.2017.
 * <p/>
 * Validation utils for route build integrity
 */
public class ValidationUtils {

    /**
     * Simple Route type validation
     *
     * @param camelRoute
     */
    public static void validateSimpleCamelRoute(CamelRoute camelRoute) {
        validateCamelRoute(camelRoute);
        OutputChoice outputChoice = camelRoute.getOutputChoices().get(0);
        Assert.notNull(outputChoice);
        Assert.notNull(outputChoice.getOutputs());
        Assert.isTrue(outputChoice.getOutputs().size() > 0);
    }

    /**
     * Choice Route type validation
     *
     * @param camelRoute
     */
    public static void validateChoiceCamelRoute(CamelRoute camelRoute) {
        validateCamelRoute(camelRoute);

        for (OutputChoice outputChoice : camelRoute.getOutputChoices()) {
            validateOutputChoice(outputChoice);
            Assert.notNull(outputChoice.getExpression());
        }
    }

    /**
     * Standard validation for all route types
     *
     * @param camelRoute
     */
    private static void validateCamelRoute(CamelRoute camelRoute) {
        Assert.notNull(camelRoute);
        Assert.notNull(camelRoute.getFrom(), "camelRoute.from is null");
        Assert.notNull(camelRoute.getRouteId(), "camelRoute.routeid is null");
        Assert.notNull(camelRoute.getOutputChoices(), "camelRoute.outputs is null");
        Assert.isTrue(camelRoute.getOutputChoices().size() > 0, "camelRoute.outputs is empty");
    }

    /**
     * Validate OutputChoice
     *
     * @param outputChoice
     */
    private static void validateOutputChoice(OutputChoice outputChoice) {
        Assert.notNull(outputChoice.getSimpleBody());
        Assert.notNull(outputChoice.getOutputs());
        Assert.isTrue(outputChoice.getOutputs().size() > 0);
    }

}
