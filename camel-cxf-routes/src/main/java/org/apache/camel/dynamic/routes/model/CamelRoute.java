package org.apache.camel.dynamic.routes.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by szagoret on 27.02.2017.
 */
@XmlRootElement(name = "route")
public class CamelRoute {

    private String routeId;

    private String from;

    private List<OutputChoice> outputChoices;

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<OutputChoice> getOutputChoices() {
        return outputChoices;
    }

    public void setOutputChoices(List<OutputChoice> outputChoices) {
        this.outputChoices = outputChoices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CamelRoute that = (CamelRoute) o;

        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (from != null ? !from.equals(that.from) : that.from != null) return false;
        return !(outputChoices != null ? !outputChoices.equals(that.outputChoices) : that.outputChoices != null);

    }

    @Override
    public int hashCode() {
        int result = routeId != null ? routeId.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (outputChoices != null ? outputChoices.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CamelRoute{" +
                "routeId='" + routeId + '\'' +
                ", from='" + from + '\'' +
                ", outputChoices=" + outputChoices +
                '}';
    }


}

