package org.apache.camel.dynamic.routes.model;

import java.util.List;

/**
 * Created by szagoret on 27.02.2017.
 */
public class OutputChoice {

    private String expression;
    private String simpleBody;
    private List<String> outputs;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getSimpleBody() {
        return simpleBody;
    }

    public void setSimpleBody(String simpleBody) {
        this.simpleBody = simpleBody;
    }

    public List<String> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<String> outputs) {
        this.outputs = outputs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OutputChoice that = (OutputChoice) o;

        if (expression != null ? !expression.equals(that.expression) : that.expression != null) return false;
        if (simpleBody != null ? !simpleBody.equals(that.simpleBody) : that.simpleBody != null) return false;
        return !(outputs != null ? !outputs.equals(that.outputs) : that.outputs != null);

    }

    @Override
    public int hashCode() {
        int result = expression != null ? expression.hashCode() : 0;
        result = 31 * result + (simpleBody != null ? simpleBody.hashCode() : 0);
        result = 31 * result + (outputs != null ? outputs.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OutputChoice{" +
                "expression='" + expression + '\'' +
                ", simpleBody='" + simpleBody + '\'' +
                ", outputs=" + outputs +
                '}';
    }
}
