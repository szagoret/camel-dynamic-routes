package org.apache.camel.dynamic.routes.service;

import org.osgi.framework.BundleContext;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by szagoret on 23.02.2017.
 */
public interface FilePersistenceService {

    void persistToFile(BundleContext bundleContext, String fileContent) throws IOException;

    void persistToFile(BundleContext bundleContext, String fileContent, String folderName, String fileName) throws IOException;

    InputStream loadPersistedRoutes(BundleContext bundleContext) throws IOException;

    InputStream loadPersistedRoutes(BundleContext bundleContext, String folderName, String fileName) throws IOException;
}
