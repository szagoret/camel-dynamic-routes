package org.apache.camel.dynamic.routes.service;

import org.osgi.framework.BundleContext;

import java.io.*;
import java.nio.file.Files;

/**
 * Created by szagoret on 23.02.2017.
 */
public class FilePersistenceServiceImpl implements FilePersistenceService {

    private static final String FOLDER_NAME = "deploy";
    private static final String FILE_NAME = "camel-routes.xml";
    private static final String EMPTY_STRING = "";

    @Override
    public void persistToFile(BundleContext bundleContext, String fileContent) throws IOException {
        persistToFile(bundleContext, fileContent, FOLDER_NAME, FILE_NAME);
    }

    @Override
    public synchronized void persistToFile(BundleContext bundleContext, String fileContent, String folderName, String fileName)
            throws IOException {

        if (fileContent == null || fileContent.equals(EMPTY_STRING)) {
            return;
        }

        /**
         * Creates a File object for a file in the persistent storage area
         * provided for the bundle by the Framework.
         */
        File dataFile = bundleContext.getDataFile(folderName);
        if (!dataFile.exists()) {
            dataFile.mkdir();
        }
        File file = new File(dataFile.getCanonicalPath() + File.separator + fileName);
        if (!file.exists()) {
            file.createNewFile();
        }

        /**
         * Write file content in the bundle persistence storage;
         * if file exists, overwrite content
         */
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            byte[] contentInBytes = fileContent.getBytes();
            fileOutputStream.write(contentInBytes);
            fileOutputStream.flush();
        }
    }

    /**
     * Load routes from persisted xml
     * <p/>
     * if the file does not exists then return an empty stream
     */
    @Override
    public InputStream loadPersistedRoutes(BundleContext bundleContext, String folderName, String fileName) throws IOException {
        File dataFile = bundleContext.getDataFile(folderName);
        File file = new File(dataFile.getCanonicalPath() + File.separator + fileName);
        byte[] bytes = file.exists() ? Files.readAllBytes(file.toPath()) : new byte[0];
        return new ByteArrayInputStream(bytes);
    }


    @Override
    public InputStream loadPersistedRoutes(BundleContext bundleContext) throws IOException {
        return loadPersistedRoutes(bundleContext, FOLDER_NAME, FILE_NAME);
    }
}
