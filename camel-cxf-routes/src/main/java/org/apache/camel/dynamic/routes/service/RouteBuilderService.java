package org.apache.camel.dynamic.routes.service;

import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.model.RouteDefinition;

import java.util.List;

/**
 * Created by szagoret on 03.02.2017.
 */
public interface RouteBuilderService {
    String addRoute(CamelRoute baseCamelRoute) throws Exception;

    List<RouteDefinition> showRoutes();

    void deleteRoute(String id) throws Exception;
}
