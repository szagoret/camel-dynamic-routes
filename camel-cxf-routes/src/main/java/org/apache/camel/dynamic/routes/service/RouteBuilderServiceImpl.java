package org.apache.camel.dynamic.routes.service;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dynamic.routes.builder.CamelRouteBuilderFactory;
import org.apache.camel.dynamic.routes.model.CamelRoute;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.model.RoutesDefinition;
import org.apache.camel.osgi.OsgiSpringCamelContext;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.InitializingBean;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by szagoret on 03.02.2017.
 */
public class RouteBuilderServiceImpl implements RouteBuilderService, InitializingBean {

    private OsgiSpringCamelContext osgiSpringCamelContext;

    private BundleContext bundleContext;

    private FilePersistenceService filePersistenceService;

    @Override
    public String addRoute(CamelRoute camelRoute) throws Exception {
        RouteBuilder routeBuilder = CamelRouteBuilderFactory.getRouteBuilder(camelRoute);
        osgiSpringCamelContext.addRoutes(routeBuilder);
        filePersistenceService.persistToFile(bundleContext, osgiSpringCamelContext.getManagedCamelContext().dumpRoutesAsXml());
        return camelRoute.getRouteId();
    }

    @Override
    public List<RouteDefinition> showRoutes() {
        return osgiSpringCamelContext.getRouteDefinitions();
    }

    @Override
    public void deleteRoute(String id) throws Exception {
        osgiSpringCamelContext.shutdownRoute(id);
        osgiSpringCamelContext.removeRoute(id);
        filePersistenceService.persistToFile(bundleContext, osgiSpringCamelContext.getManagedCamelContext().dumpRoutesAsXml());
    }

    public void setOsgiSpringCamelContext(OsgiSpringCamelContext osgiSpringCamelContext) {
        this.osgiSpringCamelContext = osgiSpringCamelContext;

    }

    public void setFilePersistenceService(FilePersistenceService filePersistenceService) {
        this.filePersistenceService = filePersistenceService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        /**
         * make accessible bundleContext field in OsgiSpringCamelContext class
         * to extract field reference
         */
        Field bundleContextField = osgiSpringCamelContext.getClass().getDeclaredField("bundleContext");
        bundleContextField.setAccessible(true);
        this.bundleContext = (BundleContext) bundleContextField.get(osgiSpringCamelContext);
        bundleContextField.setAccessible(false);

        /**
         * load persisted routes to camel context
         */
        // TODO test with different conditions:
        // if file is empty
        InputStream persistedRoutesInputStream = filePersistenceService.loadPersistedRoutes(this.bundleContext);

        if (persistedRoutesInputStream.available() > 0) {
            RoutesDefinition routeDefinition = osgiSpringCamelContext.loadRoutesDefinition(persistedRoutesInputStream);
            osgiSpringCamelContext.addRouteDefinitions(routeDefinition.getRoutes());
        }
    }
}
